//This is the header file used in all of the functions. This
//uses the stdio, string, stdlib, and math libraries.
//This header also has the prototypes for the functions.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

void intro(int low, int high);

int predict(int low, int high);

char getAnswer();
