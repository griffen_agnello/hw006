/*Griffen Agnello	CSE224		10/29/20
This my high-low game in C. The program may accept 0,1,or 2 user-inputted integer arguments from the command line. If there are no
user inputted arguments, the game will run with the default low/high values of 0/100 respectively. If one argument is provided, the high
value is changed to that integer. If two are provided, then the low value followed by the high value will be changed to the respective
input values. If the inputs are illegal or there are more than 2 user arguments, the program will close. The program will begin by
greeting the user, and asking for the user to THINK of a number between the low and high values. Based on the low/high values, the
computer will be able to guess the correct number in X number of guesses. The computer makes a guess by summing the high and low
values and then dividing the sum by 2. The user is then asked if the guess was too high, too low, or correct. If too high, the
high value will be set to the current guess value minus 1. If too low, the low value will be set to the current guess value plus 1.
If correct, the game will close. By the final guess, the computer should be able to guess the number correctly. If the guess is still
"incorrect", then the computer will assume the user is cheating and then exit.*/
#include "main.h"
void main(int argc, char* argv[]) {
        int high = 100;		//If no user command line arguments, 0 and 100 are the default settings.
        int low = 0;
	int i, prediction, guess;
	char input;
        if(argc > 3) {
                printf("ERROR, only 2 user arguments accepted.\n");	//One argument for the low value and one argument for the high
                exit (0);						//value are accepted.
        }
        if(argc == 2) {
                if(1 != sscanf(argv[1], "%d", &high) || high < 1) {		//If one user argument, this checks that the input
                        printf("ERROR, only integers above 0 are accepted.\n"); //is legal and greater than 0.
                        exit (0);
                }
        }
        if(argc == 3) {									//If 2 user arguments, this checks:
                if(1 != sscanf(argv[1], "%d", &low) || 1 != sscanf(argv[2], "%d", &high)) { // 1. Inputs are legal
                        printf("ERROR, integers only are accepted.\n");			    // 2. Low is at least equal to 0
                        exit (0);							    // 3. High is greater than 0
                }									    // 4. Low is not greater than high
                if( low < 0 || high < 1 ) {
                        printf("ERROR, low value must be at least 0 and high value must be an integer greater than 0. Goodbye\n");
                        exit (0);
                }
                if( low > high) {
                        printf("ERROR, low value may not be greater than high value. Goodbye.\n");
                        exit (0);
                }
        }
	intro(low, high);			//Executes the intro function which greets the user and asks them to think of a number.
	prediction = predict(low, high);	//Assigns the maximum number of guesses from the predict function to the prediction variable.
	printf("\nI can correctly guess your number with %d attempts.\n",prediction);	
	
	for(i=1; i <= prediction; i++)	{		//i is a counter that is initialized to 1. This represents the guess number. "i" 
		printf("GUESS #%d \n",i);		
		guess = ((high + low) / 2);		//Adds the low and high values and then divides the sum by 2 to receive the guess value
		printf("Is %d your number?\n", guess);
		input = getAnswer();			//returns either an h, l, or c based on the user's input
		if(input == 'h') {
			high = (guess - 1);		//If the input is h, the guess was too high. This sets the new high to be the guess value minus 1.
		}
		if(input == 'l') {			//If the input is l, the guess was too low. This sets the new low to be the guess value plus 1.
			low = (guess +1);
		}
		if(input == 'c') {			//If the input is c, the guess was correct. The game will end.
			printf("I guessed correctly! Thanks for playing.\n");
			exit(0);
		}
		if(low > high)	{			//If the low value manages to be higher than the high value, the game will end, calling the user a cheater!
			printf("Attention all gamers, we have a cheater right here!\n");
			exit(0);
		}
	}
}		//Thanks for reading!
