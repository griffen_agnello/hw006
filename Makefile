main: main.o getAnswer.o predict.o intro.o 
	gcc -o main main.o getAnswer.o predict.o intro.o -lm
main.o: main.c main.h
	gcc -c main.c
getAnswer.o: getAnswer.c main.h
	gcc -c getAnswer.c
predict.o: predict.c main.h
	gcc -c predict.c
intro.o: intro.c main.h
	gcc -c intro.c
clean:
	rm main.o getAnswer.o predict.o intro.o main
