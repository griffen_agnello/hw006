#include "main.h"
//This is the function that greets the user at the beginning of the game.
void intro(int low, int high) {
	char str[10];		
	printf("Welcome to the high/low game! \nThink of a number between %d and %d.", low, high);
	printf("Press enter when you have decided your number...");
	fgets(str,2, stdin);		//Greets user and then asks the user to press enter. The
					//value that is stored from the user is placed in a str variable.
					//This variable value will not be used for anything.
}
	
